import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ToastAndroid } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import Login from './src/pages/Login';
import Dashboard from './src/pages/Dashboard';
import * as firebase from 'firebase';
import { initializeApp } from 'firebase/app';
import firebaseConfig from './src/firebaseConfig';
import ProfileScreen from './src/pages/ProfileScreen';
import ActivityScreen from './src/pages/ActivityScreen';
import OnboardingScreen from './src/pages/OnboardingScreen';
import 'react-native-gesture-handler';

const Stack = createNativeStackNavigator();

export default function App() {

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Onboarding'>
        <Stack.Screen name = 'Onboarding' component={OnboardingScreen} options={{ headerShown: false}} />               
        <Stack.Screen name='Login' component={Login} options={{ headerShown: true }} />
        <Stack.Screen name='Dashboard' component={Dashboard} options={{ headerShown: true }} />
        <Stack.Screen name='Profile' component={ProfileScreen} options={{ headerShown: true }} />
        <Stack.Screen name='Activity' component={ActivityScreen} options={{ headerShown: true }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  previw: {
    alignSelf: 'stretch',
    flex: 1
  },
  button: {
    backgroundColor: '#0782F9',
    width: '60%',
    padding: 15,
    borderRadius: 10,
    alignItems: 'center',
    marginTop: 40,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  },
});
