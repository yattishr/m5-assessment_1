import { AsyncStorage, StyleSheet, Text, View, Image } from 'react-native'
import Onboarding from 'react-native-onboarding-swiper';
import React from 'react'

const OnboardingScreen = props => {
  
    const completeOnboarding = async () => {
        await AsyncStorage.setItem('hasOnboarded', JSON.stringify({ hasOnboarded: true}));
        // Navigate to Login
        props.navigation.navigate('Login')
    }
  
    return (
    <Onboarding
    onDone = {completeOnboarding}
    onSkip = {completeOnboarding}
    pages={[
      {
        backgroundColor: '#ce93d8',
        image: <Image source={require('../images/login_background.jpg')} style={{ width: '80%', height: '60%', backgroundColor: 'red', zIndex: 122 }}/>,
        title: 'Welcome to Travel Life!',
        subtitle: 'Your travel companion',
      },   
      {
        backgroundColor: '#ce93d8',
        image: <Image source={require('../images/login_background.jpg')} style={{ width: '80%', height: '60%', backgroundColor: 'red', zIndex: 122 }}/>,
        title: 'Welcome to Travel Life!',
        subtitle: 'Where do you want to go to?',
      },
      {
        backgroundColor: '#ce93d8',
        image: <Image source={require('../images/login_background.jpg')} style={{ width: '80%', height: '60%', backgroundColor: 'red', zIndex: 122 }}/>,
        title: 'Welcome to Travel Life!',
        subtitle: 'Let us take you there!',
      },
    ]}
  />
  )
}

export default OnboardingScreen

const styles = StyleSheet.create({})