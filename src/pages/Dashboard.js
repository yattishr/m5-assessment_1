import { StyleSheet, Text, View, TextInput, Button, Row, Alert, TouchableOpacity, Dimensions, Image, DrawerLayoutAndroid, FlatList } from 'react-native'
import React, { useRef, useState } from 'react'
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  SafeAreaView,
  SafeAreaProvider,
  SafeAreaInsetsContext,
  useSafeAreaInsets,
  initialWindowMetrics,
} from 'react-native-safe-area-context';
import Ionicons from '@expo/vector-icons/Ionicons';
import { auth } from '../firebaseConfig';

// // import screens
// import ActivityScreen from './ActivityScreen';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


const Dashboard = props => {

  const navigation = useNavigation()

  const NavigateToDetails = props => {
    props.navigation.navigate('Activity');
  }

  const handleSignOut = () => {
    auth
      .signOut()
      .then(() => {
        navigation.replace("Login")
      }).catch(error => {
        Alert.alert("Authentication Error.", error.message)
      })
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={handleSignOut} style={styles.button}>
        <Text style={styles.buttonText}>Sign out</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => NavigateToDetails(props) } style={styles.button}>
        <Text style={styles.buttonText}>Go to Activities</Text>
      </TouchableOpacity>
    </View>


  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    padding: 16
  },

  navigationContainer: {
    backgroundColor: "#ecf0f1"
  },

  paragraph: {
    padding: 16,
    fontSize: 15,
    textAlign: "center"
  },

  item: {
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: '700',
  },
  button: {
    backgroundColor: '#0782F9',
    width: '60%',
    padding: 15,
    borderRadius: 10,
    alignItems: 'center',
    marginTop: 40,
  },
  buttonText: {
    color: 'white',
    fontWeight: '700',
    fontSize: 16,
  }

});

export default Dashboard

