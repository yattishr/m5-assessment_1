import { StyleSheet, Text, View, TextInput, Button, Row, Alert, TouchableOpacity, Dimensions, Image, KeyboardAvoidingView } from 'react-native'
import { React, useEffect, useState } from 'react'
import {
    SafeAreaView,
    SafeAreaProvider,
    SafeAreaInsetsContext,
    useSafeAreaInsets,
    initialWindowMetrics,
} from 'react-native-safe-area-context';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import * as firebase from 'firebase';

import { auth } from '../firebaseConfig';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const Login = ({ navigation }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // const navigation = useNavigation()

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(user => {
            if (user) {
                navigation.replace('Dashboard')                
            }
        })
        return unsubscribe
    }, [])

    const handleSignUP = () => {
        auth
        .createUserWithEmailAndPassword(email, password)
        .then(userCredentials => {
            const user = userCredentials.user;
            const currentUser = userCredentials.user;;
            const db = firebase.firestore()
            db.collection('user').doc(currentUser.uid).set({
                email: email,
                password: password, 
            })
            console.log('Registered with: ', user.email)
        }).catch(error => {
            // Alert.alert("Sign in Error.", "Email and Password cannot be empty. Enter your email and password to register.")
            Alert.alert("Sign in Error.", error.message)
        })
    }

    const handleLogin = () => {
        auth
            .signInWithEmailAndPassword(email, password)
            .then(userCredentials => {
                const user = userCredentials.user;
                console.log('Logged in with: ', user.email)
            }).catch(error => {
                Alert.alert("Sign in Error.", error.message)
                console.log(`error when signing in....${error.message}`)
            })            
    }

    return (

        <KeyboardAvoidingView style={styles.container} behavior="padding">
            <View style={styles.inputContainer}>
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    name='email'
                    value={email}
                    onChangeText={text => {setEmail(text)}}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    name='password'
                    value={password}
                    secureTextEntry={true}
                    onChangeText={text => {setPassword(text)}}
                />
            </View>

            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={handleLogin}
                style={styles.button}>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={handleSignUP} style={[styles.button, styles.buttonOutline]}>
                    <Text style={styles.buttonOutlineText}>Register</Text>
                </TouchableOpacity>
            </View>
            {/* <View>
                <Text style={styles.text}
                    onPress={() => navigation.navigate('Create')}
                >Don't have an account? Click to sign up.</Text>
            </View> */}
        </KeyboardAvoidingView>

    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ce93d8',
        alignItems: 'center',
        justifyContent: 'center',
    },

    inputContainer: {
        width: '80%',
    },

    input: {
        backgroundColor: 'white',
        paddingHorizontal: 15,
        paddingVertical: 10,
        borderRadius: 10,
        marginTop: 5,
    },    

    buttonContainer: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
    },

    button: {
        backgroundColor: '#0782F9',
        width: '100%',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center'
    },

    buttonOutline: {
        backgroundColor: 'white',
        marginTop: 5,
        borderColor: '#0782F9',
        borderWidth: 2,
    },        

    buttonText: {
        color: 'white',
        fontWeight: '700',
        fontSize: 16,
    },


    buttonOutlineText: {
        color: '#0782F9',
        fontWeight: '700',
        fontSize: 16,
    }

});