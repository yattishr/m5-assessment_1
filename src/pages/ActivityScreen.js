import { StyleSheet, Text, View, TextInput, Button, Row, Alert, TouchableOpacity, Dimensions, Image, DrawerLayoutAndroid, FlatList } from 'react-native'
import { useEffect, useRef, useState } from 'react';
import { Camera } from 'expo-camera';
import { shareAsync } from 'expo-sharing';
import * as MediaLibrary from 'expo-media-library';
import { SafeAreaView } from 'react-native-safe-area-context';

const ActivityScreen = props => {
    let cameraRef = useRef();
    const [hasCameraPermission, setHasCameraPermission] = useState();
    const [hasMediaLibraryPermission, setHasMediaLibraryPermission] = useState();
    const [photo, setPhoto] = useState();

    const cameraPermissionFunction = async () => {
        const cameraPermission = await Camera.requestCameraPermissionsAsync();
        setHasCameraPermission(cameraPermission.status === "granted");
        const mediaLibraryPermission = await MediaLibrary.requestPermissionsAsync();
        setHasMediaLibraryPermission(mediaLibraryPermission.status === "granted");
    }

    useEffect(() => {
        cameraPermissionFunction();
    }, []);

    if (hasCameraPermission === undefined) {
        return <Text>Requesting camera permissions...</Text>
    } else if (!hasCameraPermission) {
        return <Text>Permission for camera not granted. Please change this in settings.</Text>
    }

    let takePic = async () => {
        let options = {
            quality: 1,
            base64: true,
            exif: false
        };

        let newPhoto = await cameraRef.current.takePictureAsync(options);
        setPhoto(newPhoto);
    };

    if (photo) {
        let sharePic = () => {
            shareAsync(photo.uri).then(() => {
                setPhoto(undefined);
            })
        };


        let savePhoto = () => {
            MediaLibrary.saveToLibraryAsync(photo.uri).then(() => {
                setPhoto(undefined);
            })
        };

        return (
            <SafeAreaView style={styles.container}>
                <Image style={styles.previw} source={{ uri: "data:image/jpg;base64" + photo.base64 }} />
                <TouchableOpacity onPress={sharePic} style={styles.button}>
                    <Text style={styles.buttonText}>Share Photo</Text>
                </TouchableOpacity>

                {hasMediaLibraryPermission ?
                    <TouchableOpacity onPress={savePhoto} style={styles.button}>
                        <Text style={styles.buttonText}>Save Photo</Text>
                    </TouchableOpacity>
                    : undefined}

                <TouchableOpacity onPress={() => setPhoto(undefined)} style={styles.button}>
                    <Text style={styles.buttonText}>Discard</Text>
                </TouchableOpacity>

            </SafeAreaView>
        )
    }

    return (
        <Camera style={styles.container} ref={cameraRef}>
            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={takePic} style={styles.button}>
                    <Text style={styles.buttonText}>Take a picture</Text>
                </TouchableOpacity>
            </View>
        </Camera>
    )
}

export default ActivityScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        padding: 16
    },
    button: {
        backgroundColor: '#0782F9',
        width: '60%',
        padding: 15,
        borderRadius: 10,
        alignItems: 'center',
        marginTop: 40,
    },
    buttonText: {
        color: 'white',
        fontWeight: '700',
        fontSize: 16,
    },
    buttonContainer: {
        width: '60%',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
    },
})