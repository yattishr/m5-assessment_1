import * as firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyB3MZE_vE6T2JGIo-NcUK2ODw7NIsQEOJM",
    authDomain: "mtn-module-5-8f388.firebaseapp.com",
    projectId: "mtn-module-5-8f388",
    storageBucket: "mtn-module-5-8f388.appspot.com",
    messagingSenderId: "106774497746",
    appId: "1:106774497746:web:f58010e49562c357ece00b"
  };


// initialize Firebase
let app;
if(firebase.apps.length === 0) {
  app = firebase.initializeApp(firebaseConfig)
} else {
  app = firebase.app()
}

const auth = firebase.auth()

export { auth }